import React from "react";
//import { Route, Switch, Redirect } from "react-router";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import MatchMaking from "../routes/MatchMaking";
import CardSelection from "../routes/CardSelection";
import Game from "../routes/Game";
import Chat from "../routes/Chat";
import { Grid } from "semantic-ui-react";

const routes = [
  {
    path: "/game",
    component: Game,
  },
  {
    path: "/matchmaking",
    component: MatchMaking,
  },
  {
    path: "/card-selection",
    component: CardSelection,
  },
];

const displayChat = true;

export default function ApplicationRouter() {
  return (
    <Router>
      <Grid columns={2}>
        <Switch>
          <Grid.Column width={displayChat ? 13 : 16}>
            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                component={route.component}
              />
            ))}
          </Grid.Column>
        </Switch>
        <Route path={"/test"} component={CardSelection} />
        {displayChat && (
          <Grid.Column width={3}>
            <Chat />
          </Grid.Column>
        )}
        <Route exact path="/">
          <Redirect to="/matchmaking" />
        </Route>
      </Grid>
    </Router>
  );
}
