import React, { useState } from "react";

import CardList from "../../core/components/CardList";
import { Button, Grid } from "semantic-ui-react";

const cards = [1, 2, 3, 4, 5, 6];

const CardExampleCard = () => {
  const [collectionCard, setColectionCard] = useState(cards);
  const [selectedCard, setSelectedCard] = useState([]);

  const handleSelectCard = (id) => {
    if (selectedCard.length > 4) return;
    setSelectedCard([...selectedCard, id]);
    setColectionCard(collectionCard.filter((card) => card != id));
  };

  const handleRemoveCard = (id) => {
    setColectionCard([...collectionCard, id]);
    setSelectedCard(selectedCard.filter((card) => card != id));
  };

  const handleValidation = () => {
    console.log("validate");
  };

  return (
    <div>
      <Grid columns={2} divided>
        <Grid.Column>
          <CardList cards={collectionCard} onClick={handleSelectCard} />
        </Grid.Column>
        <Grid.Column>
          <CardList cards={selectedCard} onClick={handleRemoveCard} />
        </Grid.Column>
      </Grid>
      <Grid centered>
        <Button disabled={selectedCard.length !== 4} onClick={handleValidation}>
          Select Cards
        </Button>
      </Grid>
    </div>
  );
};

export default CardExampleCard;
