import { JOIN_ROOM, ADD_MESSAGE_TO_ROOM, SELECT_ROOM } from "./action-types.js";

const initialState = {
  currentRoom: 0,
  rooms: [
    {
      id: 0,
      name: "général",
      user: [],
      messages: ["gfuj", "ygiudgs"],
      userTyping: [],
    },
  ],
};

const addItem = (array, item) => {
  return [...array, item];
};

const updateItemAtIndex = (state, item, index) => {
  return [
    ...state.rooms.slice(0, index),
    item,
    ...state.rooms.slice(index + 1),
  ];
};

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case JOIN_ROOM:
    case ADD_MESSAGE_TO_ROOM:
      const roomIndex = state.rooms.findIndex(
        (room) => room.id === action.roomId
      );
      const test = {
        ...state,
        rooms: [
          ...state.rooms.slice(0, roomIndex),
          {
            ...state.rooms[roomIndex],
            messages: [...state.rooms[roomIndex].messages, action.message],
          },
          ...state.rooms.slice(roomIndex + 1),
        ],
      };
      return test;
    case SELECT_ROOM:
      return { ...state, currentRoom: action.roomId };

    default:
      return state;
  }
};

export default reducer;
