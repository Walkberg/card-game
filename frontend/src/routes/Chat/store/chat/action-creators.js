import { JOIN_ROOM, ADD_MESSAGE_TO_ROOM, SELECT_ROOM } from "./action-types";

const message = 3;

export function joinRoom(user) {
  return {
    type: JOIN_ROOM,
    message,
  };
}

export function selectRoom(roomId) {
  console.log(roomId);
  return {
    type: SELECT_ROOM,
    roomId,
  };
}

export function addMessageToRoom(roomId, message) {
  console.log(roomId);
  return {
    type: ADD_MESSAGE_TO_ROOM,
    roomId,
    message,
  };
}
