import React, { useState, useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";
import MessageList from "./components/MessageList";
import MessageForm from "./components/MessageForm";
import MessageHeader from "./components/MessageHeader";

import { selectRoom, addMessageToRoom } from "./store/chat";

import { socket } from "../../core/config/socketIO";

const Chat = () => {
  const [message, setMessage] = useState("");
  const [isTyping, setIsTyping] = useState(false);

  const { currentRoomId } = useSelector((state) => ({
    currentRoomId: state.chat.currentRoom,
  }));

  const { rooms } = useSelector((state) => ({
    rooms: state.chat.rooms,
  }));

  const dispatch = useDispatch();

  useEffect(() => {
    socket.on("message", (message) => addMessage(message));
    socket.on("join_user", (message) => addMessage(message));
  }, []);

  const handleSendMessage = (e) => {
    e.preventDefault();
    socket.emit("send_message", message);
  };

  const handleChange = (e) => {
    setMessage(e.target.value);

    if (!isTyping) typing();
    socket.emit("nouveau_client", "user");
  };

  const typing = () => {
    setIsTyping(true);
    socket.emit("start_typing", message);
  };

  const addMessage = (message) => {
    dispatch(addMessageToRoom(currentRoomId, message));
  };

  const handleSelectRoom = (roomId) => {
    dispatch(selectRoom(roomId));
  };

  const getCurrentRoomAtIndex = () => {
    return rooms.findIndex((room) => room.id === currentRoomId);
  };

  const getCurrentRoom = () => {
    return rooms[getCurrentRoomAtIndex()];
  };

  const getMessageFromCurrentRoom = () => {
    return getCurrentRoom().messages;
  };

  return (
    <div>
      <MessageHeader
        rooms={rooms}
        selectedId={currentRoomId}
        onClick={handleSelectRoom}
      />
      <MessageList messages={getMessageFromCurrentRoom()} />
      {"a user is typping"}
      <MessageForm
        message={message}
        onChange={handleChange}
        onSend={handleSendMessage}
      />
    </div>
  );
};

export default Chat;
