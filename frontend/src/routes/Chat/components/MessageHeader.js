import React, { useState } from "react";
import { Menu } from "semantic-ui-react";
import { useSelector } from "react-redux";

const MessageHeader = ({ rooms, selectedId, onClick }) => {
  console.log(selectedId);
  return (
    <Menu widths={rooms.length}>
      {rooms.map((room) => (
        <Menu.Item
          key={room.name}
          name={room.name}
          active={selectedId === room.id}
          onClick={() => onClick(room.id)}
        />
      ))}
    </Menu>
  );
};

export default MessageHeader;
