import React from "react";
import { Grid } from "semantic-ui-react";
import Message from "./Message";

const MessageList = ({ messages }) => {
  return (
    <Grid.Row>
      {messages.map((message) => (
        <Message message={message} self={message % 2} />
      ))}
    </Grid.Row>
  );
};

export default MessageList;
