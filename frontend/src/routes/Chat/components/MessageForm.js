import React from "react";
import { Input } from "semantic-ui-react";

const Message = ({ onSend, message, onChange }) => {
  return (
    <form onSubmit={onSend}>
      <Input
        fluid
        focus
        onChange={onChange}
        placeholder="Type here..."
        icon={{ name: "search", link: true }}
        value={message}
      />
    </form>
  );
};

export default Message;
