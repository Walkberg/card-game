import React from "react";
import { Message } from "semantic-ui-react";

const MessageItem = ({ message, self }) => {
  return <Message color={self ? "blue" : "green"}>{message}</Message>;
};

export default MessageItem;
