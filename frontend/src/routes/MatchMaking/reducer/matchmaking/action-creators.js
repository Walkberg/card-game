import {
  JOIN_ROOM,
  CREATE_ROOM,
  DELETE_ROOM,
  SET_ROOM_LIST,
} from "./action-types";

export function setRoomList(rooms) {
  return {
    type: SET_ROOM_LIST,
    rooms,
  };
}

export function joinRoom(roomId) {
  return {
    type: JOIN_ROOM,
    roomId,
  };
}

export function createRoom(room) {
  return {
    type: CREATE_ROOM,
    room,
  };
}

export function deleteRoom(roomName) {
  return {
    type: DELETE_ROOM,
    roomName,
  };
}
