import {
  JOIN_ROOM,
  CREATE_ROOM,
  DELETE_ROOM,
  SET_ROOM_LIST,
} from "./action-types.js";

const initialState = {
  currentRoom: -1,
  rooms: [],
};

const room = { name: "test", players: [] };

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case JOIN_ROOM:
      return { ...state, currentRoom: action.roomId };
    case CREATE_ROOM:
      return {
        ...state,
        currentRoom: action.room.name,
        rooms: [...state.rooms, action.room],
      };
    case DELETE_ROOM:
      return { ...state, rooms: [...state.rooms, room] };
    case SET_ROOM_LIST:
      return { ...state, rooms: action.rooms };
    default:
      return state;
  }
};

export default reducer;
