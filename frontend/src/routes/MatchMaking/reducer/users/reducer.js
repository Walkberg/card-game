import { ADD_USER, REMOVE_USER, SET_USER_LIST } from "./action-types.js";

const initialState = {
  users: [],
};

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_USER_LIST:
      return { ...state, users: action.users };
    case ADD_USER:
      return { ...state, users: [...state.users, action.user] };
    case REMOVE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.id !== action.userId),
      };
    default:
      return state;
  }
};

export default reducer;
