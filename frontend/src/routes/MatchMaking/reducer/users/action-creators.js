import { ADD_USER, REMOVE_USER, SET_USER_LIST } from "./action-types";

export function setUsersList(users) {
  return {
    type: SET_USER_LIST,
    users,
  };
}

export function addUser(user) {
  return {
    type: ADD_USER,
    user,
  };
}

export function removeUser(userId) {
  return {
    type: REMOVE_USER,
    userId,
  };
}
