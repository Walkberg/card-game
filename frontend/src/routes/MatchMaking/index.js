import React, { useState, useEffect } from "react";
import { Button, Loader, Grid, Input } from "semantic-ui-react";
import RoomList from "./components/RoomList";

import { createRoom, joinRoom, setRoomList } from "./reducer/matchmaking";

import { addUser, removeUser, setUsersList } from "./reducer/users";
import { useDispatch, useSelector } from "react-redux";

import { socket } from "../../core/config/socketIO";

const MatchMaking = () => {
  const [roomName, setRoomName] = useState("");
  const { rooms, roomSelected } = useSelector((state) => ({
    rooms: state.matchmaking.rooms,
    roomSelected: state.matchmaking.currentRoom,
  }));

  const { users } = useSelector((state) => ({
    users: state.users.users,
  }));

  useEffect(() => {
    socket.emit("ask_rooms");
    socket.on("room_created", (room) => {
      dispatch(createRoom(room));
    });

    socket.on("ask_rooms", (rooms) => {
      dispatch(setRoomList(rooms));
    });
  }, []);

  //TODO: Move in higher component
  useEffect(() => {
    socket.emit("ask_users");
    socket.on("ask_users", (users) => {
      console.log(users);
      dispatch(setUsersList(users));
    });

    socket.on("user_joined", (user) => {
      console.log("user join");
      dispatch(addUser(user));
    });

    socket.on("user_leave", (userId) => {
      dispatch(removeUser(rooms));
    });
  }, []);

  const dispatch = useDispatch();

  const handleCreateRoom = () => {
    socket.emit("create_room", roomName);
    setRoomName("");
  };

  const handleChange = (e) => {
    setRoomName(e.target.value);
  };

  const handleClickJoin = (roomName) => {
    console.log("joinRoom" + roomName);
  };

  const handleClickSpectate = (roomName) => {
    console.log("joinRoom" + roomName);
  };

  return (
    <Grid columns={2} divided>
      <Grid.Column centered>
        <Input
          placeholder="Search..."
          value={roomName}
          onChange={handleChange}
        />
        {console.log(users)}
        <Button onClick={handleCreateRoom}>Create</Button>
      </Grid.Column>
      <Grid.Column>
        il y a {users.length} users conneté
        <RoomList
          rooms={rooms}
          selected={roomSelected}
          onClickJoin={handleClickJoin}
          onClickSpectate={handleClickSpectate}
        />
      </Grid.Column>
    </Grid>
  );
};

export default MatchMaking;
