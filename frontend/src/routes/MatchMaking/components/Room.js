import React from "react";
import { Table, List, Button } from "semantic-ui-react";

const Room = ({ room, onClickJoin, onClickSpectate, selected }) => {
  return (
    <Table.Row positive={selected}>
      <Table.Cell>{room.name} </Table.Cell>
      <Table.Cell>(0/2)</Table.Cell>
      <Table.Cell>
        <Button onClick={onClickJoin} primary>
          Join
        </Button>
        <Button onClick={onClickSpectate}>Spectate</Button>
      </Table.Cell>
    </Table.Row>
  );
};

export default Room;
