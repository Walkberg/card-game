import React from "react";
import Room from "./Room";
import { Label, List, Table } from "semantic-ui-react";

const RoomList = ({ rooms, onClickJoin, onClickSpectate, selected }) => {
  return (
    <Table divided selection>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Player</Table.HeaderCell>
          <Table.HeaderCell>Join</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      {rooms.map((room) => (
        <Room
          selected={selected === room.name}
          room={room}
          onClickJoin={onClickJoin}
          onClickSpectate={onClickSpectate}
        />
      ))}
    </Table>
  );
};

export default RoomList;
