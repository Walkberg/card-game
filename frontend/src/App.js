import logo from "./logo.svg";
import "./App.css";
import Router from "./routes/mainRouter";
import "semantic-ui-css/semantic.min.css";

import { Provider } from "react-redux";
import { store } from "./core/store";

function App() {
  return (
    <Provider store={store}>
      <Router />
    </Provider>
  );
}

export default App;
