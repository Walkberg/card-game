import io from "socket.io-client";

//allow cors
let socket = io(process.env.REACT_APP_TEST, { transports: ["websocket"] });

export { socket };
