import { createStore, combineReducers } from "redux";

import { chatReducer } from "../routes/Chat/store/chat";
import { matchmakingReducer } from "../routes/MatchMaking/reducer/matchmaking";
import { userReducer } from "../routes/MatchMaking/reducer/users";

const combineReducer = combineReducers({
  chat: chatReducer,
  matchmaking: matchmakingReducer,
  users: userReducer,
});

const store = createStore(combineReducer);

export { store };
