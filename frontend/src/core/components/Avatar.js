import React from "react";
import { Image } from "semantic-ui-react";

const Avatar = () => (
  <div>
    <Image src="../../asset/images/user.svg" avatar />
    <span>Username</span>
  </div>
);

export default Avatar;
