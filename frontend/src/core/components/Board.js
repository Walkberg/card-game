import React from "react";
import { Card, Grid } from "semantic-ui-react";
import BoardSide from "./BoardSide";
import Separation from "./Separation";

const Board = () => {
  return (
    <Grid row={3}>
      <Grid.Row>
        <BoardSide />
      </Grid.Row>
      <Grid.Row>
        <Separation />
      </Grid.Row>
      <Grid.Row>
        <BoardSide />
      </Grid.Row>
    </Grid>
  );
};

export default Board;
