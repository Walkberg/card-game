import React from "react";
import { Grid } from "semantic-ui-react";

import CardList from "./CardList";
import Avatar from "./Avatar";

const BoardSide = ({ cards, onClick }) => {
  return (
    <Grid columns={3}>
      <Grid.Column width={3}>
        <Avatar />
      </Grid.Column>
      <Grid.Column width={13}>
        <CardList cards={[1, 2, 3, 4]} onClick={onClick} />
      </Grid.Column>
    </Grid>
  );
};

export default BoardSide;
