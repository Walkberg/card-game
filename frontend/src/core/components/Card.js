import React from "react";
import { Card, Icon, Image } from "semantic-ui-react";

const CardExampleCard = ({ onClick }) => (
  <Card onClick={onClick} color="red">
    <Image
      src="https://www.cdiscount.com/pdt2/7/8/8/1/1200x1200/asm3701103703788/rw/carte-pokemon-35-108-pikachu-niv-12-60-pv-xy-evo.jpg"
      wrapped
      ui={false}
    />
    <Card.Content>
      <Card.Header>Matthew</Card.Header>
      <Card.Meta>
        <span className="date">Joined in 2015</span>
      </Card.Meta>
      <Card.Description>
        Matthew is a musician living in Nashville.
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <a>
        <Icon name="user" />
        22 Friends
      </a>
    </Card.Content>
  </Card>
);

export default CardExampleCard;
