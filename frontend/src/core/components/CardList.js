import React from "react";
import { Card } from "semantic-ui-react";
import CardItem from "./Card";

const CardList = ({ cards, onClick }) => (
  <Card.Group>
    {cards.map((card, index) => (
      <CardItem key={index} onClick={() => onClick(card)} />
    ))}
  </Card.Group>
);

export default CardList;
