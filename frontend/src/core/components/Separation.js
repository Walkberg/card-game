import React from "react";
import { Image, Button } from "semantic-ui-react";

const Separation = () => (
  <div>
    <Button>End turn</Button>
  </div>
);

export default Separation;
