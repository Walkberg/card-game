const stompit = require('stompit');
 var MessageSending = function MessageSending(){};
MessageSending.prototype.run = function run(message){
    stompit.connect({ host: 'localhost', port: 61613 }, (err, client) => {
    const frame = client.send({ destination: 'ChatMessageQueue' });
    
    frame.write(message);
    console.log("Message sent to the queue");
    frame.end();
    
    client.disconnect();
    });
}

module.exports = new MessageSending();