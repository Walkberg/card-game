const Room = require("./room");

class RoomList {
  constructor() {
    this.rooms = [];
  }

  hasRoom(roomName) {
    const index = this.rooms.findIndex((room) => room.name === roomName);
    console.log(index);
    return index < 0;
  }

  getByName(roomName) {
    const index = this.rooms.findIndex((room) => room.name === roomName);
    return this.rooms[index];
  }

  createRoom(roomName) {
    if (!this.hasRoom(roomName)) return;
    const room = new Room(roomName);
    this.rooms.push(room);
  }
}

module.exports = RoomList;
