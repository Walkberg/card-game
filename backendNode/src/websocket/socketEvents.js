const User = require("../models/User");
const RoomList = require("../models/RoomList");

const rooms = new RoomList();
const users = [];

exports = module.exports = function (io) {
  io.on("connection", (socket) => {
    console.log(`Connecté au client ${socket.id} mdr`);

    let currentUser = new User(1, socket.id, "test");
    users.push(currentUser);
    io.emit("user_joined", currentUser);

    socket.on("nouveau_client", function (pseudo) {
      console.log("fugdsiufgsidfu");
    });

    socket.on("send_message", (message) => {
      console.log(message);
      io.emit("message", message);
    });

    socket.on("start_typing", () => {});

    socket.on("end_typing", () => {});

    socket.on("join_room", () => {});

    socket.on("ask_rooms", () => {
      socket.emit("ask_rooms", rooms.rooms);
    });

    socket.on("ask_users", () => {
      console.log(users);
      socket.emit("ask_users", users);
    });

    socket.on("create_room", (roomName) => {
      rooms.createRoom(roomName);
      io.emit("room_created", rooms.getByName(roomName));
      console.log(rooms.rooms);
    });

    socket.on("delete_room", () => {});

    socket.on("disconnect", () => {
      io.emit("user_leave", currentUser.id);
    });
  });
};
