const cors = require("cors");
var app = require("express")();
var server = require("http").createServer(app);
const io = require("socket.io")(server);

const port = process.env.PORT || 4001;

let socketEvents = require("./src/websocket/socketEvents")(io);

// Chargement de la page index.html
app.get("/", function (req, res) {
  res.sendfile(__dirname + "/index.html");
});

server.listen(port, () => console.log(`Listening on port ${port}`));
